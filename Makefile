
CC = gcc
CFLAGS = -Os -Wall

.SUFFIXES:

.PHONY: all clean

%.o:%.c ; $(CC) -c -o $@ $<

%:%.o ; $(CC) -o $@ $<

all: rtprio

clean:
	rm -f -- rtprio rtprio.o
