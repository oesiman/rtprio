/* (c) 2000-2002 by Marc Lehmann <pcg@goof.com>
 * minor fixes (c) 2002 by St. Traby <stefan@hello-penguin.com>
 */

// GPL 2.0 or later


#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>

#include <sched.h>

#ifndef SCHED_IDLE
# define SCHED_IDLE 3
#endif

void
usage (void)
{
	fprintf (stderr, "rtprio [ -r | -o | -f | -i ] [-n prio] [-p pid | command [args ...]]\n");
	fprintf (stderr, "rtprio -q[pid]\n");
	exit (1);
}

int
main (int argc, char *argv[])
{
	int i, res;
	struct sched_param param;
	int pid = 0;
	int sched = SCHED_FIFO;

	param.sched_priority = 50;

        opterr = 1;
	while ((i = getopt (argc, argv, "+rofiq::n:p:")) != EOF) {
		switch (i) {
		case 'r':
			sched = SCHED_RR;
			break;
		case 'o':
			sched = SCHED_OTHER;
			param.sched_priority = 0;
			break;
		case 'f':
			sched = SCHED_FIFO;
			break;
		case 'i':
			sched = SCHED_IDLE;
			param.sched_priority = 1;
			break;
		case 'q':
                        if(optarg)
                          pid = atoi (optarg);
			sched = -1;
			break;
		case 'n':
			param.sched_priority = atoi (optarg);
			break;
		case 'p':
			pid = atoi (optarg);
			break;
		default:
			usage ();
		};
	};

	argc -= optind;
	argv += optind;

	if ((pid || sched < 0) && argc > 0) {
		fprintf (stderr, "cannot specify commands AND a pid/query\n");
		exit (1);
	}
	if (!pid)
		pid = getpid ();	/* for better error-messages */

	if (sched < 0) {	/* query */
		if (((res = sched_getscheduler (pid)) < 0) || (sched_getparam (pid, &param) < 0)) {
			fprintf (stderr, "process %d does not exist\n", pid);
			exit (1);
		} else {
			switch (res) {
			case SCHED_IDLE:
				printf ("process %d, SCHED_IDLE, priority %d\n", pid, param.sched_priority);
				break;
			case SCHED_OTHER:
				printf ("process %d, SCHED_OTHER, priority %d\n", pid, param.sched_priority);
				break;
			case SCHED_FIFO:
				printf ("process %d, SCHED_FIFO, priority %d\n", pid, param.sched_priority);
				break;
			case SCHED_RR:
				printf ("process %d, SCHED_RR, priority %d\n", pid, param.sched_priority);
				break;
			default:
				printf ("process %d, unknown scheduler, priority %d\n", pid, param.sched_priority);
				break;
			}
		}
	} else {
		if (sched_setscheduler (pid, sched, &param) < 0) {
			fprintf (stderr, "cannot set scheduling policy of process %d: ", pid);
			perror (""); // avoid %m
			exit (1);
		}
		if (argc > 0) {
			execvp (argv[0], &argv[0]);
                        fprintf(stderr, "can't exec \"%s\": ", argv[0]);
			perror (""); // avoid %m
                        exit(1);
		}
	}

	return 0;
}
